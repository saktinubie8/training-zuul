package id.co.telkomsigma.training.zuulproxy;

import com.netflix.util.Pair;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletResponse;

@Configuration
public class LocationHeaderRewritingFilter extends ZuulFilter {
    private UrlPathHelper urlPathHelper = new UrlPathHelper();
    private RouteLocator routeLocator;
	
    public LocationHeaderRewritingFilter(RouteLocator routeLocator) {
        this.routeLocator = routeLocator;
    }
    
	@Override
	public boolean shouldFilter() {	
		if(locationHeader(RequestContext.getCurrentContext())!=null)
			return true;
		return false;
	}

	@Override
	public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        Route route = routeLocator.getMatchingRoute(urlPathHelper.getPathWithinApplication(ctx.getRequest()));
        if (route != null) {
        	
            Pair<String, String> lh = locationHeader(ctx);
            String upstream = route.getFullPath();
            String downstream = ServletUriComponentsBuilder.fromCurrentContextPath().path(route.getPrefix()).build().toUriString();
            if(lh.second().startsWith("http://")){
            	String urlWithNoHttp = lh.second().substring(7);
                System.out.println(urlWithNoHttp);
            	int index = urlWithNoHttp.indexOf("/");
            	System.out.println(index);
            	String path = urlWithNoHttp.substring(index);
            	System.out.println(path);
            	lh.setSecond(downstream+path);
            }else if(lh.second().startsWith("https://")){
            	String urlWithNoHttps = lh.second().substring(8);
            	int index = urlWithNoHttps.indexOf("/");
            	String path = urlWithNoHttps.substring(index);
            	lh.setSecond(downstream+path);
            }
			HttpServletResponse servletResponse = ctx.getResponse();
            servletResponse.addHeader("content-encoding","gzip");
            
}
        return null;
	}

	@Override
	public String filterType() {
		// TODO Auto-generated method stub
		return "post";
	}

	@Override
	public int filterOrder() {
		// TODO Auto-generated method stub
		return 100;
	}

    private Pair<String,String> locationHeader(RequestContext ctx) {
    	for(Pair<String,String> par:ctx.getZuulResponseHeaders())
    	{
    		if("Location".equals(par.first()))
    			return par;
    	}
        return null;
}
}
